# Bank Account Kata
Ce projet est un exercice pour les étudiants en Product Owner, basé sur le kata du compte bancaire.

## Objectif
L'objectif de cet exercice est de mettre en pratique les concepts clés de la gestion de projet, tels que la collecte des besoins, la rédaction de user stories et la communication avec les développeurs et le client.

### Description
Le projet a pour objectif de concevoir un système de compte bancaire simple qui permettra aux utilisateurs de réaliser des opérations courantes telles que le dépôt, le retrait d'argent et la consultation de leur historique de transactions. En outre, des propositions pour l'ajout de fonctionnalités complémentaires pourront être envisagées.

## Exercice
Écrire un cahier des charges pour une application bancaire destinée aux clients d'une banque fictive appelée ABC Bank. L'application doit offrir des fonctionnalités de base de dépôt, de retrait et de consultation de l'historique de compte.

### Tâches à effectuer
- Décrivez brièvement l'objectif de l'application bancaire et les fonctionnalités qu'elle devrait offrir.
- Énumérez les exigences techniques pour l'application, y compris la technologie à utiliser, la sécurité, la convivialité et la disponibilité.
- Décrivez les règles de gestion pour l'application, y compris les règles de gestion pour les comptes bancaires, les transactions et les utilisateurs.
- Décrivez les fonctionnalités supplémentaires que vous aimeriez ajouter à l'application.
- Décrivez les fonctionnalités que vous aimeriez ajouter à l'application si vous aviez plus de temps.
- Décrivez les fonctionnalités que vous aimeriez ajouter à l'application si vous aviez plus de budget.
- Élaborez un plan de livraison pour le projet, y compris les détails sur la façon dont le projet sera livré, le budget et le délai.
- Précisez les détails sur la formation et le support technique à fournir aux employés de la banque. 
- Décrivez les responsabilités de l'équipe de développement et la procédure de validation de l'application.

### Consignes
Les étudiants doivent travailler en solo ou en équipe de 2 personnes.
Chaque équipe doit nommer un chef de projet responsable de la coordination des travaux et de la soumission du résultat final.
Le document final doit être clairement structuré et facile à lire.
Les équipes ont 24h pour compléter l'exercice.

### Critères de notation

- [ ] Le document est clairement structuré et facile à lire.
- [ ] Le document contient une description détaillée de l'application et de ses fonctionnalités.
- [ ] Le document contient une liste détaillée des exigences techniques.
- [ ] Le document présente les différentes règles de gestion.
- [ ] Le document contient un plan de livraison détaillé.
- [ ] Le document contient des détails sur la formation et le support technique.
- [ ] Le document contient une description détaillée des responsabilités de l'équipe de développement et de la procédure de validation de l'application.
- [ ] Le document est soumis avant la date limite.
- [ ] Le document est soumis dans le bon format Readme de préférence sinon (PDF, DOC, DOCX, ODT, etc.).
- [ ] Le document est soumis dans le bon dépôt une branche DEV de préference.
